/*
 * projection.hpp
 *
 *  Created on: 07-May-2015
 *      Author: chinmaya
 */

#ifndef SOURCE_PROJECTION_HPP_
#define SOURCE_PROJECTION_HPP_

#include <cmath>
#include <vector>
#include <cassert>
#include <limits>
#include <fstream>
#include <algorithm>
#include <map>

#include <CGAL/Aff_transformation_3.h>
#include <CGAL/Vector_3.h>
#include <CGAL/Direction_3.h>
#include <CGAL/Point_3.h>
#include <CGAL/Kernel/global_functions_3.h>
#include <CGAL/Kernel/global_functions_2.h>
#include <CGAL/Kernel/global_functions.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Vector_3.h>
#include <CGAL/Segment_3.h>
#include <CGAL/Triangle_3.h>
#include <CGAL/intersections.h>

#include <boost/tuple/tuple.hpp>

#include "utilities.hpp"

const double PI = 3.1415926535897;

/*
template <typename Iterator, typename Point_Map_In, typename Point_Out_Map, typename Point_3, typename Vector_3>
void change_camera(Iterator begin, Iterator end, Point_Map_In point_map, Point_Map)
*/

template <typename Kernel>
CGAL::Direction_3<Kernel> get_old_x_direction(double angle_degrees)
{
	double radians = angle_degrees * PI / 180;
	return CGAL::Direction_3<Kernel>(std::cos(radians), 0, std::sin(radians));
}

template <typename Kernel, typename Direction_3>
CGAL::Aff_transformation_3<Kernel> get_axis_change_transformation(Direction_3 x, Direction_3 y)
{
	typedef CGAL::Aff_transformation_3<Kernel> Transformation;
	Direction_3 z (CGAL::cross_product(x.vector(), y.vector()));
	// FIXME It looks like dx(), etc. don't necessarily return values
	// corresponding to a unit vector. This is fine when used
	// with get_old_x_direction.
	Transformation t(x.dx(), y.dx(), z.dx(),
			x.dy(), y.dy(), z.dy(),
			x.dz(), y.dz(), z.dz());
	return t;
}

template <typename Kernel, typename Iterator_In, typename Point_Map_In, typename Point_Map_Out, typename Point_3, typename Direction_3>
void change_camera(Iterator_In begin, Iterator_In end,
		Point_Map_In in_map, Point_Map_Out out_map,
		Point_3 old_origin, Direction_3 old_x, Direction_3 old_y)
{
	typedef CGAL::Aff_transformation_3<Kernel> Transformation;
	typedef CGAL::Vector_3<Kernel> Vector;
	Transformation t = get_axis_change_transformation<Kernel>(old_x, old_y);
	Vector v(Point_3(0, 0, 0), old_origin);

	for (Iterator_In it = begin; it != end; it++)
	{
		Point_3 p_old = get(in_map, *it);
		Point_3 p_new = p_old + v;
		p_new = p_new.transform(t);
		put(out_map, *it, p_new);
	}
}

/**
 * Returns in the format [q_i, q_j, q_k, q_r] where q_r is the real part
 */
template <typename K>
std::map<char, typename K::FT> get_quaternion_from_axis_angle(typename K::Vector_3 unit_axis_vector, typename K::FT angle)
{
	std::map<char, typename K::FT> quaternion;
	quaternion['i'] = unit_axis_vector.x() * std::sin(angle/2);
	quaternion['j'] = unit_axis_vector.y() * std::sin(angle/2);
	quaternion['k'] = unit_axis_vector.z() * std::sin(angle/2);
	quaternion['r'] = std::cos(angle/2);

	return quaternion;
}

template <typename K>
std::vector<typename K::FT> get_rotation_matrix_from_quaternion(std::map<char, typename K::FT> q)
{
	std::vector<typename K::FT> matrix;

	matrix.push_back(1 - 2 * q['j'] * q['j'] - 2 * q['k'] * q['k']);
	matrix.push_back(2 * (q['i'] * q['j'] - q['k'] * q['r']));
	matrix.push_back(2 * (q['i'] * q['k'] + q['j'] * q['r']));

	matrix.push_back(2 * (q['i'] * q['j'] + q['k'] * q['r']));
	matrix.push_back(1 - 2 * q['i'] * q['i'] - 2 * q['k'] * q['k']);
	matrix.push_back(2 * (q['j'] * q['k'] - q['i'] * q['r']));

	matrix.push_back(2 * (q['i'] * q['k'] - q['j'] * q['r']));
	matrix.push_back(2 * (q['j'] * q['k'] + q['i'] * q['r']));
	matrix.push_back(1 - 2 * q['i'] * q['i'] - 2 * q['j'] * q['j']);

	return matrix;
}

template <typename K, typename Iterator, typename Point_Map>
void apply_rotation_matrix(Iterator begin, Iterator end, Point_Map pmap, std::vector<typename K::FT> matrix)
{
	typedef typename K::Aff_transformation_3 Transformation;
	typedef typename K::Point_3 Point;
	Transformation t(matrix[0], matrix[1], matrix[2],
			matrix[3], matrix[4], matrix[5],
			matrix[6], matrix[7], matrix[8]);

	for (Iterator it = begin; it != end; it++)
	{
		Point p_old = get(pmap, *it);
		Point p_new = p_old.transform(t);
		std::cout << "Old point: " << p_old << " Rotated point: " << p_new << std::endl;
		put(pmap, *it, p_new);
	}
}

/*template <typename K, typename Iterator, typename Point_Map>
void change_camera()*/

template <typename K>
class Cell
{
public:
	typedef CGAL::Point_3<K> Point_3;
	typedef typename CGAL::Polyhedron_3<K>::Facet_handle Facet_handle;
	typedef CGAL::Vector_3<K> Vector_3;
	typedef typename CGAL::Polyhedron_3<K>::Vertex_handle Vertex_handle;
	// Any changes to EnrichedPoint should be made in Main.cpp too
	/*index, Point_3, normal, Polyhedron Vertex, projection cell, isContour, isO, color*/
	typedef boost::tuple<int, Point_3, Vector_3, Vertex_handle, Cell<K>*, bool, bool, boost::tuple<int, int, int>> EnrichedPoint;
	Point_3 top_left;
	double length;
	double height;
	int depth;
	int row_index;
	int column_index;

	// Connections
	Cell* parent;
	std::vector<Cell*> children;

	std::vector<Facet_handle> contained_facets;
	std::vector<EnrichedPoint*> contained_points;

	Cell()
	{
		top_left = Point_3(0, 0, 0);
		length = 0;
		height = 0;
		depth = 0;
		parent = NULL;
		row_index = 0;
		column_index = 0;
	}

	Cell(Point_3 top_left, double length, double height, Cell* parent, int depth)
	{
		this->top_left = top_left;
		this->length = length;
		this->height = height;
		this->parent = parent;
		this->depth = depth;
		this->row_index = 0;
		this->column_index = 0;
	}

	Cell(Point_3 top_left, double length, double height, Cell* parent, int depth, int column_index, int row_index)
	{
		this->top_left = top_left;
		this->length = length;
		this->height = height;
		this->parent = parent;
		this->depth = depth;
		this->row_index = row_index;
		this->column_index = column_index;
	}

	std::vector<Cell*>& subdivide(int horizontal_divisions, int vertical_divisions)
	{
		double child_length = length / horizontal_divisions;
		double child_height = height / vertical_divisions;
		Vector_3 right(child_length, 0, 0);
		Vector_3 down(0, -child_height, 0);

		int child_column_offset = column_index * horizontal_divisions;
		int child_row_offset = row_index * vertical_divisions;

		for (int i = 0; i < horizontal_divisions; i++)
		{
			for (int j = 0; j < vertical_divisions; j++)
			{
//				std::cout << "i: " << i << ", right: " << right << ", i * right: " << i * right << std::endl;
//				std::cout << "j: " << j << ", down: " << down << ", j * down: " << j * down<< std::endl;
				Point_3 child_top_left = top_left + (i * right) + (j * down);
//				std::cout << "child_top_left: " << child_top_left << std::endl;
				Cell* child = new Cell(child_top_left, child_length, child_height, this, depth + 1, child_column_offset + i, child_row_offset + j);
				children.push_back(child);
			}
		}
		return children;
	}

	std::size_t num_children()
	{
		return children.size();
	}

	typename std::vector<Cell*>::iterator children_begin()
	{
		return children.begin();
	}

	typename std::vector<Cell*>::iterator children_end()
	{
		return children.end();
	}

	typename std::vector<Facet_handle>::iterator facet_begin()
	{
		return contained_facets.begin();
	}

	typename std::vector<Facet_handle>::iterator facet_end()
	{
		return contained_facets.end();
	}

	typename std::vector<EnrichedPoint*>::iterator points_begin()
	{
		return contained_points.begin();
	}

	typename std::vector<EnrichedPoint*>::iterator points_end()
	{
		return contained_points.end();
	}

	Cell* get_parent()
	{
		return parent;
	}

	bool contains_projection(Point_3 p)
	{
		// Top and left are inclusive, bottom and right are exclusive
		return (p.x() >= top_left.x())
				&& (p.x() < (top_left.x() + length))
				&& (p.y() <= top_left.y())
				&& (p.y() > (top_left.y() - height));
	}

	bool contains_projection(Facet_handle f)
	{
		bool result = true;
		assert (f->degree() >= 3);
		auto circulator = f->facet_begin();
		do
		{
			result &= (contains_projection(circulator->vertex()->point()));
		}
		while (++circulator != f->facet_begin());
		return result;
	}
};

// todo return all levels as vectors instead of just the bottom one
/**
 * @return Returns a vector of all the "leaves" of the grid
 */
template <typename K>
std::vector<Cell<K>*> make_grid(CGAL::Point_3<K> top_left, double length, double height, int horizontal_subdivisions, int vertical_subdivisions, int depth)
{
	Cell<K>* initial = new Cell<K>(top_left, length, height, NULL, 0);
	std::vector<Cell<K>*> grid;
	grid.push_back(initial);
	for (int i = 0; i < depth; i++)
	{
		// cache the size as it will change
		// and we only need the initial value
		std::size_t initial_size = grid.size();
		for (std::size_t j = 0; j < initial_size; ++j)
		{
			std::vector<Cell<K>*> children = (grid[j])->subdivide(horizontal_subdivisions, vertical_subdivisions);
			// append children to the original vector
			grid.insert(grid.end(), children.cbegin(), children.cend());
		}
		grid.erase(grid.begin(), grid.begin() + initial_size);
	}
	return grid;
}

/**
 * This function assumes that the grid elements have been properly indexed already
 */
template <typename K, typename Iterator>
std::vector<std::vector<Cell<K>*>> make_2d_vector_of_grid_leaves(Iterator begin, Iterator end, int num_rows, int num_columns)
{
	std::vector<std::vector<Cell<K>*>> grid;
	grid.resize(num_rows);
	for (auto it = grid.begin(); it != grid.end(); it++)
	{
		it->resize(num_columns);
	}
	for (auto it = begin; it != end; it++)
	{
		grid[(*it)->row_index][(*it)->column_index] = *it;
	}
	return grid;
}

template <typename K>
Cell<K>* find_root(Cell<K>* node)
{
	int level = node->depth;
	while (node->get_parent())
	{
		node = node->get_parent();
		assert(node->depth == (--level));
	}

	return node;
}

template <typename K, typename CellIterator, typename Element>
int find_cell_for_element(Element e, CellIterator begin, CellIterator end)
{
//	std::cout << "find_cell_for_element() begin" << std::endl;
	int i = 0;
	for (auto it = begin; it != end; ++it)
	{
		if ((*it)->contains_projection(e))
		{
//			std::cout << "found cell for element, i: " << i << std::endl;
			return i;
		}
		i++;
	}
	return -1;
}

template <typename K>
void assign_facets_to_cells(CGAL::Polyhedron_3<K>& p, Cell<K>* node)
{
	int num_assigned = 0;
	Cell<K>* root = find_root(node);
	for (auto it = p.facets_begin(); it != p.facets_end(); it++)
	{
//		std::cout << "assign_facets_to_cells() outer loop" << std::endl;
		assert(root->contains_projection(it));
		Cell<K>* assigned = root;
		int index;
		while ((index = find_cell_for_element<K>(it, assigned->children_begin(), assigned->children_end())) != -1)
		{
//			std::cout << "assign_facets_to_cells() inner loop" << std::endl;
			assigned = assigned->children[index];
		}
		assigned->contained_facets.push_back(it);
		num_assigned++;
	}
	std::cout << "Assigned " << num_assigned << "facets" << std::endl;
}

template <typename Point, typename Triangle>
bool is_point_triangle_vertex(Point p, Triangle t)
{
	return (p == t[0])
			|| (p == t[1])
			|| (p == t[2]);
}

template <typename K, typename Facet_handle>
CGAL::Triangle_3<K> triangle_from_facet(Facet_handle f)
{
	typedef CGAL::Triangle_3<K> Triangle;
	auto fb = f->facet_begin();
	return Triangle(fb->vertex()->point(),
			fb->next()->vertex()->point(),
			fb->next()->next()->vertex()->point());
}

template <typename K, typename Point>
bool is_visible(Point p, Cell<K>* node)
{
	if (p.z() == 0)
	{
		return true;
	}
	typedef CGAL::Segment_3<K> Segment;
	typedef CGAL::Triangle_3<K> Triangle;
	Segment s(p, Point(p.x(), p.y(), 0));

	while (node)
	{
		for (auto it = node->facet_begin(); it != node->facet_end(); ++it)
		{
			Triangle tri = triangle_from_facet<K>(*it);
//			std::cout << "Intersecting: " << tri << ", " << s << std::endl;
			auto inter = CGAL::intersection(tri, s);
			if (inter && (!is_point_triangle_vertex(p, tri)))
			{
				return false;
			}
		}
		node = node->parent;
	}
	return true;
}

template <typename K, typename Iterator, typename Point_Map_In, typename Cell_Map_Out>
void assign_vertices_to_cells(Iterator begin, Iterator end, Point_Map_In pmap, Cell_Map_Out cmap, Cell<K>* node)
{
	int num_assigned = 0;
	Cell<K>* root = find_root(node);
	for (auto it = begin; it != end; it++)
	{
//		std::cout << "assign_vertices_to_cells() outer loop" << std::endl;
		CGAL::Point_3<K> p = get(pmap, *it);
		assert(root->contains_projection(p));
		Cell<K>* assigned = root;
		int index;
		while ((index = find_cell_for_element<K>(p, assigned->children_begin(), assigned->children_end())) != -1)
		{
//			std::cout << "assign_vertices_to_cells() inner loop" << std::endl;
			assigned = assigned->children[index];
		}
		if (is_visible(p, assigned))
		{
			assigned->contained_points.push_back(&(*it)); // get a real pointer instead of the iterator
			put(cmap, *it, assigned);
			num_assigned++;
		}
		else
		{
			put(cmap, *it, NULL);
		}
//		std::cout << "Processed: " << (it - begin) << std::endl;
	}
	std::cout << "Assigned " << num_assigned << " vertices" << std::endl;
}

template <typename K, typename Point_Map_Out, typename Iterator>
void assign_infinity_points(Iterator begin, Iterator end, Point_Map_Out ep_point_map, double bounding_box_max_z)
{
	typedef CGAL::Point_3<K> Point;
	typedef CGAL::Vector_3<K> Vector;
	typedef typename Cell<K>::EnrichedPoint EnrichedPoint;

	Vector displace_to_infinity(0, 0, bounding_box_max_z + 0.1);
	int num_assigned = 0;
	for (auto it = begin; it != end; it++)
	{
		if ((*it)->contained_points.size() == 0)
		{
			Point p = (*it)->top_left + displace_to_infinity;
			if (is_visible(p, *it))
			{
				EnrichedPoint* ep = new EnrichedPoint();
				put(ep_point_map, *ep, Point(p.x(), p.y(), std::numeric_limits<double>::infinity()));
				(*it)->contained_points.push_back(ep);
				++num_assigned;
			}
		}
	}
	std::cout << "Assigned " << num_assigned << " infinity points" << std::endl;
}

#endif /* SOURCE_PROJECTION_HPP_ */
