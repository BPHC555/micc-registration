#ifndef CONTOUR_HPP_INCLUDED
#define CONTOUR_HPP_INCLUDED

// standard libraries
#include <vector>
#include <boost/property_map/property_map.hpp>

#include <CGAL/Plane_3.h>

#include "preprocess.hpp"
#include "projection.hpp"
#include "utilities.hpp"

template <class Point_Map_In, typename InputIterator, class Point_Map_Out, typename OutputIterator, class Transformation>
void rotate(Point_Map_In in_map, InputIterator p_begin, InputIterator p_beyond,
		Point_Map_Out out_map, OutputIterator out, const Transformation& t)
{
	typedef typename CGAL::value_type_traits<OutputIterator>::type EnrichedPoint;
	std::for_each(p_begin, p_beyond,
			[t, in_map, out, out_map](typename CGAL::value_type_traits<InputIterator>::type p)
			mutable {EnrichedPoint pt; put(out_map, pt, get(in_map, p).transform(t)); *out++ = pt;});
}

template <typename K>
struct contour_t
{
	typedef typename Cell<K>::EnrichedPoint* EPPointer;
	std::vector<EPPointer> c;
	std::vector<EPPointer> o;
};

template <typename K>
std::vector<Cell<K>*> get_neighbors(std::vector<std::vector<Cell<K>*>>& grid, int row, int column, int num_rings)
{
	int grid_rows = grid.size();
	int grid_columns = grid[0].size();

	int neighbors_rows_begin = ((row - num_rings) >= 0) ? (row - num_rings) : 0;
	int neighbors_rows_end = ((row + num_rings) < grid_rows) ? (row + num_rings) : (grid_rows - 1);
	int neighbors_columns_begin = ((column - num_rings) >= 0) ? (column - num_rings) : 0;
	int neighbors_columns_end = ((column + num_rings) < grid_columns) ? (column + num_rings) : (grid_columns - 1);

	std::vector<Cell<K>*> neighbors;
	for (int i = neighbors_rows_begin; i <= neighbors_rows_end; i++)
	{
		for (int j = neighbors_columns_begin; j <= neighbors_columns_end; j++)
		{
			if ((i != row) && (j != column))
			{
				neighbors.push_back(grid[i][j]);
			}
		}
	}

	return neighbors;
}

template <typename K>
std::vector<typename Cell<K>::EnrichedPoint*> flatten_neighbors(std::vector<Cell<K>*> neighbors)
{
	std::vector<typename Cell<K>::EnrichedPoint*> flattened;
	for (auto it = neighbors.cbegin(); it != neighbors.cend(); it++)
	{
		flattened.insert(flattened.begin(), (*it)->contained_points.cbegin(), (*it)->contained_points.cend());
	}
	return flattened;
}

template <typename K, typename Point_Map_In, typename Contour_Map_Out, typename O_Map_Out>
void mark_contours(std::vector<std::vector<Cell<K>*>> grid,
		Point_Map_In ep_point_map,
		Contour_Map_Out ep_contour_map,
		O_Map_Out ep_o_map,
		double zeta = 0.05,
		int num_rings = 1)
{
	typedef typename Cell<K>::EnrichedPoint EnrichedPoint;
	typedef typename Cell<K>::Point_3 Point_3;

	std::cout << "begin" << std::endl;
	double infinity = std::numeric_limits<double>::infinity();
	int num_rows = grid.size();
	int num_columns = grid[0].size();

	// mark all borders as contours
	for (int i = 0; i < num_rows; i++)
	{
		Cell<K>* curr = grid[i][0];
		for (auto it = curr->points_begin(); it != curr->points_end(); it++)
		{
			put(ep_contour_map, **it, true);
		}
		curr = grid[i][num_columns - 1];
		for (auto it = curr->points_begin(); it != curr->points_end(); it++)
		{
			put(ep_contour_map, **it, true);
		}
	}
	std::cout << "Completed vertical" << std::endl;
	for (int i = 1; i < num_columns - 1; i++)
	{
		Cell<K>* curr = grid[0][i];
		for (auto it = curr->points_begin(); it != curr->points_end(); it++)
		{
			put(ep_contour_map, **it, true);
		}
		curr = grid[num_rows - 1][i];
		for (auto it = curr->points_begin(); it != curr->points_end(); it++)
		{
			put(ep_contour_map, **it, true);
		}
	}

	std::cout << "Completed borders" << std::endl;

	// mark others
	for (int i = 0; i < num_rows; i++)
	{
		for (int j = 0; j < num_columns; j++)
		{
			std::vector<EnrichedPoint*> curr_points = grid[i][j]->contained_points;
			if (curr_points.size() == 0)
				continue;

			if ((curr_points.size() == 1)
					&& (get(ep_point_map, *curr_points[0]).z() == infinity))
					continue;

			auto n = get_neighbors<K>(grid, i, j, num_rings);
			std::vector<EnrichedPoint*> neighbors = flatten_neighbors<K>(n);

			// we also want to compare with points in the same cell
			neighbors.insert(neighbors.end(), curr_points.begin(), curr_points.end());

			for (auto it1 = curr_points.begin(); it1 != curr_points.end(); it1++)
			{
				Point_3& p = get(ep_point_map, **it1);
				if (p.z() == infinity)
				{
					continue;
				}
				for (auto it2 = neighbors.begin(); it2 != neighbors.end(); it2++)
				{
					double delta = get(ep_point_map, **it2).z() - p.z();
					if (delta > zeta)
					{
						put(ep_contour_map, **it1, true);
					}
					else if (delta <= -zeta)
					{
						put(ep_o_map, **it1, true);
					}
				}
			}
		}
	}
}

template <typename K, typename Iterator, typename Contour_Map_In, typename O_Map_In>
contour_t<K> filter_contours(Iterator begin, Iterator end, Contour_Map_In ctmap, O_Map_In omap)
{
	contour_t<K> contour;
//	typename Cell<K>::EnrichedPoint ep;
//	contour.c.push_back(&ep);
	std::for_each(begin, end, [& contour, ctmap](typename Cell<K>::EnrichedPoint& ep) mutable { if (get(ctmap, ep)) { contour.c.push_back(&ep); } });
	std::for_each(begin, end, [& contour, omap](typename Cell<K>::EnrichedPoint& ep) mutable { if (get(omap, ep)) { contour.o.push_back(&ep); } });
	return contour;
}

template <typename K, typename O_Map_In, typename Iterator>
Iterator prune_by_occluded(Iterator begin, Iterator end, O_Map_In omap)
{
	auto erase_begin = std::remove_if(begin, end, [omap](typename contour_t<K>::EPPointer epp){ return get(omap, *epp); });
//	std::cout << "to be erased = " << c.c.end() - erase_begin << std::endl;
	return erase_begin;
}

template <typename K, typename O_Map_In>
void prune_by_occluded(contour_t<K>& c, O_Map_In omap)
{
	auto erase_begin = prune_by_occluded<K>(c.c.begin(), c.c.end(), omap);
//	std::cout << "to be erased = " << c.c.end() - erase_begin << std::endl;
	c.c.erase(erase_begin, c.c.end());
}

template <typename K, typename Normal_Map_In, typename Point_Map_In, typename Iterator>
Iterator prune_by_normal(Iterator begin, Iterator end, Normal_Map_In nmap, Point_Map_In pmap, typename Cell<K>::Point_3 other_camera)
{
	typedef typename Cell<K>::Vector_3 Vector_3;
	typedef typename Cell<K>::Point_3 Point_3;
	auto erase_begin = std::remove_if(begin, end,
			[nmap, pmap, other_camera](typename contour_t<K>::EPPointer epp) {
				Vector_3 normal = get(nmap, *epp);
				Point_3 point = get(pmap, *epp);
				Vector_3 camera_direction = Vector_3(point, other_camera);
				return (normal * camera_direction) < 0;
	});
	return erase_begin;
}

template <typename K, typename Normal_Map_In, typename Point_Map_In>
void prune_by_normal(contour_t<K>& c, Normal_Map_In nmap, Point_Map_In pmap, typename Cell<K>::Point_3 other_camera)
{
	auto erase_begin = prune_by_normal<K>(c.c.begin(), c.c.end(), nmap, pmap, other_camera);
	c.c.erase(erase_begin, c.c.end());
}

#endif // CONTOUR_HPP_INCLUDED
